import { Injectable } from '@angular/core';
import { Login } from '../app/Utilities/Entity/Login';
import { AllURL } from '../app/Utilities/API/URL';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { User } from '../app/Utilities/Entity/SignUp';
import { AddGame } from '../app/Utilities/Entity/AddGame';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs/Rx';
const httpOptionsText = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  }),
  responseType: 'text' as 'json'
};
@Injectable()
export class ConnectionService {

  constructor(private http: HttpClient) { }

  check(l: Login): Observable<any> {
    console.log('From Service Login=' + l.email);
    return this.http.post<any>(AllURL.check, l, httpOptionsText);
  }


  add(u: User): Observable<any> {

    console.log(AllURL.insert);
    console.log('From service Add method=' + u.companyName);
    return this.http.post<any>(AllURL.insert, u);

  }

  addGame(g: AddGame): Observable<any> {
     console.log('From service');
    console.log(g.img);
    console.log(g.cost);
    console.log(g.description);
    console.log(g.gameName);
    console.log(g.gameCategory);
    return this.http.post<any>(AllURL.addgame, g);
  }
}
