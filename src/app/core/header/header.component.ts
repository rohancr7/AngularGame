import { Component, OnInit } from '@angular/core';
import { Login } from '../../Utilities/Entity/Login';
import { User } from '../../Utilities/Entity/SignUp';
import { ConnectionService } from '../../connection.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { Router } from '@angular/router';
import swal from 'sweetalert2';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  form: FormGroup;
  constructor(private connectionService: ConnectionService, private router: Router) { }

  ngOnInit() {
    this.form = new FormGroup({
      // tslint:disable-next-line:max-line-length
      fname: new FormControl('', [Validators.required, Validators.pattern('[a-zA-z][a-zA-Z]+'), Validators.minLength(3), Validators.maxLength(10)]),
      // tslint:disable-next-line:max-line-length
      lname: new FormControl('', [Validators.required, Validators.pattern('[a-zA-z][a-zA-Z]+'), Validators.minLength(3), Validators.maxLength(10)]),
      // tslint:disable-next-line:max-line-length
      contact: new FormControl('', [Validators.required, Validators.pattern('^[7-9][0-9]{9}$'), Validators.minLength(10), Validators.maxLength(10)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(7), Validators.maxLength(10)]),
      cpassword: new FormControl('', [Validators.required, Validators.minLength(7), Validators.maxLength(10)]),
    });
  }
  check(email: String, password: String) {

    const login = new Login();
    login.email = email;
    login.password = password;
    this.connectionService.check(login).subscribe(
      data => {
        console.log(data);
        const obj = JSON.parse(data);
        console.log(obj.message);
        if (obj.message === 'Missing credentials') {
          swal({
            type: 'error',
            title: 'Please Fill Both EmailId And Password',
            showConfirmButton: true,
          });

        } else if (obj.message === 'Invalid EmailId') {

          swal({
            type: 'error',
            title: 'EmailId Entered is Wrong',
            showConfirmButton: true,
          });
        } else if (obj.message === 'Incorrect password') {
          swal({
            type: 'error',
            title: 'Password Entered is Wrong',
            showConfirmButton: true,
          });
        } else {
          swal({
            type: 'success',
            title: 'Succesfully logged in',
            showConfirmButton: true,
          });
          this.router.navigate(['../../usercore']);
        }
      });

  }

  addUser(form) {
    const u = new User();
    console.log('jee');
    u.firstName = this.form.value.fname;
    u.companyName = this.form.value.lname;
    u.contactNumber = this.form.value.contact;
    u.email = this.form.value.email;
    u.password = this.form.value.password;
    u.confirmPassword = this.form.value.cpassword;
    if (u.password !== u.confirmPassword) {
      swal({
        type: 'error',
        title: 'Password does not match..Please Fill Form Again and Check Both Passwords are same',
        showConfirmButton: true,
      });
    } else {
      swal({
        type: 'success',
        title: 'Succesfully logged in',
        showConfirmButton: true,
      });

      console.log(this.form.value.fname);
      this.connectionService.add(u).subscribe(
        data =>
          console.log(data)
      );
      this.router.navigate(['../../usercore']);
    }

  }

}
