import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { HeaderComponent } from './header/header.component';
import { HomepageComponent } from './homepage/homepage.component';
import { FooterComponent } from './footer/footer.component';
import { ReactiveFormsModule } from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import { MatToolbarModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import {MatButtonModule, MatInputModule, MatCardModule, MatNativeDateModule} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    CoreRoutingModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatButtonModule, MatInputModule, MatCardModule, MatNativeDateModule
  ],
  exports: [
    HeaderComponent
    , HomepageComponent
    , FooterComponent

  ],

  declarations: [HeaderComponent, HomepageComponent, FooterComponent]

})
export class CoreModule { }
