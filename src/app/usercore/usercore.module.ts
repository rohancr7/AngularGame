import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsercoreRoutingModule } from './usercore-routing.module';
import { HeaderComponent } from './header/header.component';
import { HomepageComponent } from './homepage/homepage.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';
import { GamepageComponent } from './gamepage/gamepage.component';



@NgModule({
  imports: [
    CommonModule,
    UsercoreRoutingModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatButtonModule,
    MatGridListModule
  ],
  declarations: [HeaderComponent, HomepageComponent, GamepageComponent]
})
export class UsercoreModule { }
