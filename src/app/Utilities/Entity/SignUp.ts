export class User {
    email: string;
    firstName: string;
    companyName: string;
    contactNumber: string;
    password: string;
    confirmPassword: string;
}
