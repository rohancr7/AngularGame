export class AddGame {
    gameName: String;
    gameCategory: String;
    description: String;
    img: File;
    cost: String;
}
