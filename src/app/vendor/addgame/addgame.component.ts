import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, NgForm, FormControl } from '@angular/forms';
import { ConnectionService } from '../../connection.service';
import { AddGame } from '../../Utilities/Entity/AddGame';
@Component({
  selector: 'app-addgame',
  templateUrl: './addgame.component.html',
  styleUrls: ['./addgame.component.css']
})
export class AddgameComponent implements OnInit {
  form: FormGroup;
  ngOnInit() {

    this.form = new FormGroup({
      // tslint:disable-next-line:max-line-length
      gameName: new FormControl('', [Validators.required, Validators.pattern('[a-zA-z][a-zA-Z]+'), Validators.minLength(3), Validators.maxLength(10)]),
      // tslint:disable-next-line:max-line-length
      gameCategory: new FormControl('', [Validators.required, Validators.pattern('[a-zA-z][a-zA-Z]+'), Validators.minLength(3), Validators.maxLength(10)]),
      // tslint:disable-next-line:max-line-length
      //   contact: new FormControl('', [Validators.required, Validators.pattern('^[7-9][0-9]{9}$'), Validators.minLength(10), Validators.maxLength(10)]),
      gameDescription: new FormControl('', [Validators.required, Validators.pattern('[a-zA-z][a-zA-Z]+')]),
      // tslint:disable-next-line:max-line-length
      gameCost: new FormControl('', [Validators.required, Validators.pattern('^[1][0-9]{3}$'), Validators.minLength(4), Validators.maxLength(4)])
    });
  }

  addUser(form) {
    console.log(this.form.value.gameName);
    console.log("hello");
  }



}
