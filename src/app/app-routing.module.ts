import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/core',
    pathMatch: 'full'
  },
  {
    path: 'core',
    loadChildren: './core/core.module#CoreModule'
  },
  {
    path: 'usercore',
    loadChildren: './usercore/usercore.module#UsercoreModule'
  },
  {
    path: 'vendor',
    loadChildren: './vendor/vendor.module#VendorModule'
  },
  {
    path: '**',
    redirectTo: '/core',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
