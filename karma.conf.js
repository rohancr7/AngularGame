// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular/cli','karma-typescript'],
    files: [
      { pattern: "src/**/*.ts" }
    ],
    plugins: [
      require('karma-jasmine'),
      require('karma-typescript'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('@angular/cli/plugins/karma'),
      require('karma-chrome-launcher'),
    ],
    karmaTypescriptConfig: {
      reports:
      {
        "lcovonly": {
          "directory": "coverage",
          "filename": "lcov.info",
          "subdirectory": "lcov"
        }
      }
    },
    client:{
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    coverageIstanbulReporter: {
      reports: [ 'html', 'lcovonly' ],
      fixWebpackSourcePaths: true
    },
    angularCli: {
      environment: 'dev'
    },
    reporters: ['progress', 'kjhtml',"dots","karma-typescript"],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: false
  });
};
